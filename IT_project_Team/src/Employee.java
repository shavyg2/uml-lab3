import java.util.ArrayList;

public class Employee {

	protected String employee_type;

	public ArrayList<WorkInterface> work;

	public Employee() {
		work = new ArrayList<WorkInterface>();
		employee_type = "Standard Dude";
	}

	public void do_work() {
		if (this.work.isEmpty()) {
			System.out.println("The " + this.employee_type
					+ " has no work to do");
		} else {
			for (WorkInterface w : this.work) {
				System.out.print("The " + employee_type + " ");
				w.do_work();
			}
		}

	}

	public void add_work(WorkInterface work) {
		this.work.add(work);
	}

	public void delete_work(int index) {
		work.remove(index);
	}

}

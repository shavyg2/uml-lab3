public class JobSite {

	public static void main(String[] args) {
		
		business_analyst_day();
		
		project_manager_day();

	}

	public static void business_analyst_day() {

		Employee ba = new BusinessAnalyst();


		ba.add_work(new BusinessAnalystWork());
		// tester work
		
		
		ba.add_work(new TesterWork());

		// doing work
		ba.do_work();
	}
	
	
	public static void project_manager_day() {

		Employee pm = new ProjectManager();

		// /business analyst work
		 
		pm.add_work(new ProjectManagerWork());

		// tester work
		pm.add_work(new BusinessAnalystWork());

		// doing work
		pm.do_work();
	}

}

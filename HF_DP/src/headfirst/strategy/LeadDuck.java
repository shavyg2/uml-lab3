package headfirst.strategy;

public class LeadDuck extends Duck {

	
	public LeadDuck() {
		this.swimBehavior=new SwimDrown();
		this.flyBehavior= new FlyNoWay();
	}
	
	@Override
	void display() {
		System.out.println("I'm a real Lead Duck, i'm HARD to miss");

	}

}

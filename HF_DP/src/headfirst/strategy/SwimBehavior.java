package headfirst.strategy;

public interface SwimBehavior {

	public void swim();
}
